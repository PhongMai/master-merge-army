using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMono<GameManager>
{
    private void Awake()
    {
        Init();
    }

    public async void Init()
    {
        Debug.Log("Init");
        Logic.Instance.Init();
    }

    public void StartGame(bool isStartGame)
    {
        Logic.Instance.ChessBoardLogic.StartGame(isStartGame);
    }

    public void EndFight(bool isWin)
    {
        if(isWin == true)
        {          
            Logic.Instance.PlayerLogic.Win();
        }
       UiController.Instance.ShowPopupWin(isWin);
    }
}
