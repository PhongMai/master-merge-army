using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class ChessBoard : MonoBehaviour
{
    public virtual void Setup(List<GameObject> slots)
    {
        for(int i = 0; i < slots.Count; i++)
        {
            ChessItem chessItem = slots[i].GetComponent<ChessItem>();
            chessItem.setid(i);
        }
    }

    public virtual void setDataChess(List<Chess> dataChessBoard, List<GameObject> slots , int layerChess , string layerTarget)
    {
        for (int i = 0; i < dataChessBoard.Count; i++)
        {
            ChessItem chessItem = slots[i].GetComponent<ChessItem>();
            chessItem.setData(dataChessBoard[i] , layerChess , layerTarget);
        }
    }

    public float setTotalHealth(List<Chess> dataChessBoard)
    {
        float totalHealth = 0;
        for (int i = 0; i < dataChessBoard.Count; i++)
        {
            if (dataChessBoard[i]!= null)
            {
                if (dataChessBoard[i].prefabs!= null)
                {
                    totalHealth += dataChessBoard[i].Health;
                }
            }
        }
        return totalHealth;
    }

    public void DesTroyAllGameobject(List<GameObject> slots)
    {
        foreach(var item in slots)
        {
            ChessItem chessItem = item.GetComponent<ChessItem>();
            chessItem.DesTroyPrefab();
        }
    }
}
