using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using DG.Tweening;
using static UnityEngine.GraphicsBuffer;

public class ChessController : MonoBehaviour
{
    private bool isStartGame = false;
    private string maskLayerTarget;
    private Collider2D[] Tartgets;
    private float damage, totalHp, distanceAttack, hpCurrent;
    private Chesstype type;
    private Animator animator;
    [SerializeField] BulletController BulletPrefab;
    [SerializeField] Transform PosFire;
    [SerializeField] private GameObject TextFly;
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        hpCurrent = totalHp;

    }
    void Update()
    {
        if (isStartGame)
        {
            Tartgets = Physics2D.OverlapCircleAll(transform.position, 12f, LayerMask.GetMask(maskLayerTarget));
            if (Tartgets.Length > 0)
            {
                float distance = Vector3.Distance(transform.position, getEnemyNearest().position);
                if (distance <= distanceAttack)
                {
                    transform.position = transform.position;
                    animator.SetTrigger("Attack");
                }
                else
                {
                    move();
                }
                if (hpCurrent <= 0)
                {
                    Destroy(gameObject);
                    Logic.Instance.ChessBoardLogic.updateHpBoard(totalHp, gameObject.layer);
                }
            }
            else
            {
                animator.SetTrigger("Idle");
            }
        }
    }

    public Transform getEnemyNearest()
    {
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        if(Tartgets.Length > 0)
        {
            foreach (var t in Tartgets)
            {
                float dist = Vector3.Distance(t.gameObject.transform.position, currentPos);
                if (dist < minDist)
                {
                    tMin = t.gameObject.transform;
                    minDist = dist;
                }
            }
        }        
        return tMin;
    }

    public void move()
    {
        transform.position = Vector3.MoveTowards(transform.position, Tartgets[0].transform.position, 3f* Time.deltaTime);
        animator.SetTrigger("Move");
    }
    public void idle()
    {       
        animator.SetTrigger("Idle");
    }

    public void Attack()
    {
        if (Tartgets.Length > 0)
        {
            if (type == Chesstype.Dinosaur)
            {
                getEnemyNearest().gameObject.GetComponent<ChessController>().takeDamage(damage);
            }
            else
            {
                GameObject bulletSpawn = Instantiate(BulletPrefab.gameObject, PosFire.position, Quaternion.identity);
                bulletSpawn.GetComponent<BulletController>().setDataTarget(getEnemyNearest().position, gameObject.layer, damage);
            }
        }
    }

    public void setData(float healdData, float damageData , float DistanceAttack, Chess chessData,Chesstype typeData)
    {
        totalHp = healdData;
        damage = damageData;
        distanceAttack = DistanceAttack;
        type = typeData;
    }

    public void takeDamage(float damage)
    {
        hpCurrent -= damage;
        GameObject textFlySpawn = Instantiate(TextFly, transform.position, Quaternion.identity);
        textFlySpawn.GetComponent<TextMesh>().text = damage.ToString();
        textFlySpawn.transform.DOScale(0.5f, 2);
        textFlySpawn.transform.DOMoveY(transform.position.y + 1.5f, 2f);
        Destroy(textFlySpawn, 1.5f);
    }


    public void setStatus(bool StartGame)
    {
        isStartGame = StartGame;
    }

    public void setLayer(int numberlayer , string layerTarget)
    {
        maskLayerTarget = layerTarget;
        gameObject.layer = numberlayer;
    }
}
