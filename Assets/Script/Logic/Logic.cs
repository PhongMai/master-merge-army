using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class baseLogic
{
    public Func<SaveDataManager> getSaveDataManager;
    public abstract bool save();
}
public class Logic : Singleton<Logic>
{
    public SaveDataManager saveDataManager;



    public DinosaurLogic DinosaurLogic {  get; private set; }
    public WarriorsLogic WarriorLogic { get; private set; }
    public ChessBoardLogic ChessBoardLogic { get; private set; }

    public PlayerLogic PlayerLogic { get; private set; }
    public void Init()
    {
        saveDataManager = new SaveDataManager();
        saveDataManager.Load();

        PlayerLogic = new PlayerLogic(saveDataManager.dataPlayer);
        PlayerLogic.getSaveDataManager = () => saveDataManager;

        DinosaurLogic = new DinosaurLogic(ConfigManager.Instance.DinosaurConfig,saveDataManager.dataSoldiers);
        DinosaurLogic.getSaveDataManager = () => saveDataManager;

        WarriorLogic = new WarriorsLogic(ConfigManager.Instance.WarriorConfig);
        WarriorLogic.getSaveDataManager = () => saveDataManager;

        ChessBoardLogic = new ChessBoardLogic(saveDataManager.dataChessBoard);
        ChessBoardLogic.getSaveDataManager = () => saveDataManager;

       
    }
}
