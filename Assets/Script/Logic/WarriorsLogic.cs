using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorsLogic : baseLogic
{
    private List<Chess> warriors = new List<Chess>();
    public WarriorsLogic(ChessConfig dataConfig)
    {
        clone(dataConfig.ChessList);

    }

    public void clone(List<Chess> dataConfig)
    {
        foreach (var item in dataConfig)
        {
            warriors.Add(item.clone());
        }
    }


    public List<Chess> getData()
    {
        return warriors;
    }

    public Chess getChessNextLevel(int level)
    {
        var chess = new Chess();
        foreach (var item in warriors)
        {
            if (item.Level == level + 1)
            {
                chess = item;
            }
        }
        return chess;
    }

    public override bool save()
    {
        return true;
    }
}
