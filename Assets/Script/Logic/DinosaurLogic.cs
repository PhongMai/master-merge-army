using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurLogic : baseLogic
{
    private List<Chess> dinosaurs = new List<Chess>();
    public DinosaurLogic(ChessConfig dataConfig, SaveDataSoldiders dataSoldiers)
    {
        var data = dataConfig.ChessList;
        if(dataSoldiers.ListSoldiers.Count != 0)
        {
            data = dataSoldiers.ListSoldiers;
        }
        clone(data);
    }

    public void clone(List<Chess> dataConfig)
    {
        foreach (var item in dataConfig)
        {
            dinosaurs.Add(item.clone());
        }
    }

    public Chess getChess(string id)
    {
        var chess = new Chess();
        foreach(var item in dinosaurs)
        {
            if(id == item.Id)
            {
                chess = item.clone();
                break;
            }
        }
        return chess;
    }

    public List<Chess> getData()
    {
        return dinosaurs;
    }

    public Chess getChessNextLevel(int level)
    {
        var chess = new Chess();
        foreach (var item in dinosaurs)
        {
            if(item.Level == level + 1)
            {                
                chess = item;
            }
        }
        return chess;
    }

    public void UnLockNewChess(string id)
    {
        foreach(var item in dinosaurs)
        {
            if(item.Id == id)
            {
                item.Status = Status.UnLock;
            }
        }
        save();
    }

    public override bool save()
    {
        var saveData = getSaveDataManager();
        saveData.SaveDataSoldiers(getData());
        return true;
    }
}
