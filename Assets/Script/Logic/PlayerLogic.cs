using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : baseLogic
{
    public int level { get ; private set; }
    public int Coin { get; private set; }

    public event Action<int ,int > UpdateDataPlayerCallBack;
    public PlayerLogic(SaveDataPlayer dataPlayer)
    {
        level = dataPlayer.level;
        Coin = dataPlayer.Coin;

        UpdateDataPlayerCallBack?.Invoke(Coin, level);
    }





    public void Win()
    {
        foreach(var item in ConfigManager.Instance.LevelConfig.levelDatas)
        {
            if(item.level == level)
            {
                Coin += item.CoinGet;
                break;
            }
        }
        level++;
        UpdateDataPlayerCallBack?.Invoke(Coin, level);
        save();
    }

    public void SubtractionCoin()
    {
        Coin -= 1;
        UpdateDataPlayerCallBack?.Invoke(Coin, level);
        save();
    }




    public override bool save()
    {
        var saveData = getSaveDataManager();
        saveData.setDataPlayer(level,Coin);
        return true;
    }
}
