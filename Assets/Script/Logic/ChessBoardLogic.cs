using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class ChessBoardLogic : baseLogic
{
    private Dictionary<int, Chess> chessOnBoardCurrent = new Dictionary<int, Chess>();
    private Dictionary<int, Chess> chessOnBoardEnemy = new Dictionary<int, Chess>();
    private int numberSlotBefore, numberSlotAfter;
    private GameObject chessCurrentSelect;

    public event Action<GameObject> UpdateDataChessBoardCallBack;
    public event Action<Chess> PopupNewChessCallBack;
    public event Action<bool> StartGameCallBack;
    public event Action UpdateBoardEnemyCallBack;
    public event Action<float> UpdateHpBoardPlayerCallBack;
    public event Action<float> UpdateHpBoardEnemyCallBack;
    public event Action DestroyAllPrefabCallBack;
    public ChessBoardLogic(SaveChessBoard dataSave)
    {
        for (int i = 0; i < 15; i++)
        {
            chessOnBoardCurrent.Add(i, null);
            chessOnBoardEnemy.Add(i, null);
        }
        if (dataSave.slots.Count == 0)
        {            
            chessOnBoardCurrent[0] = Logic.Instance.DinosaurLogic.getData()[0];
            setDataChessBoardEnemy();
        }
        else
        {
            for (int i = 0; i < chessOnBoardCurrent.Count; i++)
            {
                chessOnBoardCurrent[i] = dataSave.slots[i];
                chessOnBoardEnemy[i] = dataSave.EnemySlots[i];
            }            
        }
    }

    public List<Chess> getData()
    {
        return chessOnBoardCurrent.Values.ToList();
    }

    public List<Chess> getDataChessEnemy()
    {
        return chessOnBoardEnemy.Values.ToList();
    } 

    public void setDataChessBoardEnemy()
    {
        chessOnBoardEnemy.Clear();
        for(int i = 0;i < 15; i++)
        {
            chessOnBoardEnemy[i] = null;
        }
        List<string> enemyOfLevel = new List<string>();
        foreach (var item in ConfigManager.Instance.LevelConfig.levelDatas)
        {
            if (item.level == Logic.Instance.PlayerLogic.level)
            {
                enemyOfLevel = item.IdEnemys;
                break;
            }
        }
        for (int i = 0; i < enemyOfLevel.Count; i++)
        {
            bool isNull = false;            
            while (isNull == false)
            {
                int numberSlot = UnityEngine.Random.RandomRange(1, 9);
                if(chessOnBoardEnemy[numberSlot] == null)
                {
                    isNull = true;
                    chessOnBoardEnemy[numberSlot] = Logic.Instance.DinosaurLogic.getChess(enemyOfLevel[i]);
                }
            }
        }
    }

    public void MoveChessWithMouse(GameObject chessCurrent)
    {
        if(chessCurrent != null)
        {
            chessCurrentSelect = chessCurrent;
            var PointMouseInput = Input.mousePosition;
            var pointMouseView = Camera.main.ScreenToWorldPoint(PointMouseInput);
            chessCurrentSelect.transform.position = new Vector2(pointMouseView.x, pointMouseView.y);
        }        
    }

    public void MoveChessWithSlot()
    {
        if (chessCurrentSelect != null)
        {          
            if (numberSlotAfter == numberSlotBefore)
            {
                chessOnBoardCurrent[numberSlotAfter] = chessOnBoardCurrent[numberSlotAfter];
            }
            else
            {
                var chessAfter = chessOnBoardCurrent[numberSlotAfter];
                if (chessAfter != null && chessAfter.Type == chessOnBoardCurrent[numberSlotBefore].Type && chessAfter.Level == chessOnBoardCurrent[numberSlotBefore].Level && chessOnBoardCurrent[numberSlotAfter].prefabs != null)
                {
                    chessOnBoardCurrent[numberSlotBefore] = null;
                    chessOnBoardCurrent[numberSlotAfter] = levelUpChess(chessOnBoardCurrent[numberSlotAfter].Level , chessOnBoardCurrent[numberSlotAfter].Type);     
                    if(chessOnBoardCurrent[numberSlotAfter].Status == Status.Lock)
                    {
                        save();
                        Logic.Instance.DinosaurLogic.UnLockNewChess(chessOnBoardCurrent[numberSlotAfter].Id);
                        PopupNewChessCallBack?.Invoke(chessOnBoardCurrent[numberSlotAfter]);                        
                        return;
                    }
                }
                else
                {
                    chessOnBoardCurrent[numberSlotAfter] = chessOnBoardCurrent[numberSlotBefore];
                    chessOnBoardCurrent[numberSlotBefore] = chessAfter;
                }                
            }
            UpdateDataChessBoardCallBack?.Invoke(chessCurrentSelect);
            ResetData();
            save();
        }
    }

    public void setNumberBefore(int slotBefore)
    {
        numberSlotBefore = slotBefore;
    }

    public void setNumberAfter(int numberAfter)
    {
        numberSlotAfter = numberAfter;
    }

    public void ResetData()
    {
        chessCurrentSelect = null;
    }
    public void AddChess(int number)
    {
        if (Logic.Instance.PlayerLogic.Coin >= 1)
        {
            for (int i = 0; i < chessOnBoardCurrent.Count; i++)
            {
                if (chessOnBoardCurrent[i] == null || chessOnBoardCurrent[i].prefabs == null)
                {
                    if(number == 0)
                    {
                        chessOnBoardCurrent[i] = Logic.Instance.DinosaurLogic.getData()[0];
                    }
                    else
                    {
                        chessOnBoardCurrent[i] = Logic.Instance.WarriorLogic.getData()[0];
                    }
                    break;
                }
            }
            UpdateDataChessBoardCallBack?.Invoke(null);
            Logic.Instance.PlayerLogic.SubtractionCoin();
            save();
        }
    }

    public Chess levelUpChess(int level, Chesstype type)
    {
        var newChess = new Chess();
        if(type == Chesstype.Dinosaur)
        {
            var dinosaursLogic = Logic.Instance.DinosaurLogic;
            newChess = dinosaursLogic.getChessNextLevel(level);
        }
        else
        {
            var warriorsLogic = Logic.Instance.WarriorLogic;
            newChess = warriorsLogic.getChessNextLevel(level);
        }
        return newChess;
    }

    public void StartGame(bool isStartGame)
    {
        save();
        StartGameCallBack?.Invoke(isStartGame);
    }

    public void updateHpBoard(float hp,int numberlayer)
    {
        if(numberlayer == layer.Instance.numberLayerLeague)
        {
            UpdateHpBoardPlayerCallBack?.Invoke(hp);
        }
        else
        {
            UpdateHpBoardEnemyCallBack?.Invoke(hp);
        }
    }

    public void hideChess()
    {
        save();
        for (int i = 0; i < chessOnBoardCurrent.Count; i++)
        {
            chessOnBoardCurrent[i] = null;
            chessOnBoardEnemy[i] = null;
        }
        UpdateBoardEnemyCallBack?.Invoke();
        UpdateDataChessBoardCallBack?.Invoke(null);
    }

    public void resetDataChessBoard(bool isWin)
    {
        var dataSaveChessBoard = getSaveDataManager().dataChessBoard.slots;
        var dataSaveChessBoardEnemy = getSaveDataManager().dataChessBoard.EnemySlots;
        if (dataSaveChessBoard.Count == 0)
        {            
            chessOnBoardCurrent[0] = Logic.Instance.DinosaurLogic.getData()[0];
        }
        else
        {
            for (int i = 0; i < chessOnBoardCurrent.Count; i++)
            {
                chessOnBoardCurrent[i] = dataSaveChessBoard[i];                
            }
        }

        if(isWin == true)
        {
            setDataChessBoardEnemy();
        }
        else
        {
            for (int i = 0; i < chessOnBoardCurrent.Count; i++)
            {
                chessOnBoardEnemy[i] = dataSaveChessBoardEnemy[i];
            }
        }
        UpdateBoardEnemyCallBack?.Invoke();
        UpdateDataChessBoardCallBack?.Invoke(null);
    }
    public void DestroyAllPrefab()
    {
        DestroyAllPrefabCallBack?.Invoke();
    }
    public override bool save()
    {
        var saveData = getSaveDataManager();
        saveData.SaveDataSlot(chessOnBoardCurrent.Values.ToList() , chessOnBoardEnemy.Values.ToList());
        return true;
    }
}
