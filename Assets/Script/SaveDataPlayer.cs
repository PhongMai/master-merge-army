using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataPlayer : BaseSaveData
{
    public int level;
    public int Coin;
    public override string getKey()
    {
        return "W_PlaerData";
    }

    public override void LoadDefaunt()
    {
        level = 0;
        Coin = 1;
    }
}
