using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[Serializable]
public class icon
{
    public string name;
    public Sprite spriteIcon;
}


[CreateAssetMenu(fileName ="IconConfig" , menuName ="Config/IconConfig")]
public class IconConfig : ScriptableObject
{
    public List<icon> icons;    
}
