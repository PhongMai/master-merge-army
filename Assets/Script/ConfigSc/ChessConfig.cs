using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Chesstype
{
    Dinosaur,
    Warrior
}
public enum Status
{
    Lock,
    UnLock
}

[Serializable]
public class Chess
{
    public string Id;
    public int Level;
    public float Health;
    public float Damage;
    public float DistanceAtt;
    public ChessController prefabs;
    public Status Status;
    public Chesstype Type;

    public Chess clone()
    {
        var clone = new Chess();
        clone.Id = Id;
        clone.Level = Level;
        clone.Health = Health;
        clone.Damage = Damage;
        clone.DistanceAtt = DistanceAtt;
        clone.prefabs = prefabs;
        clone.Status = Status;
        clone.Type = Type;
        return clone;
    }
}

[CreateAssetMenu(fileName = "ChessConfig", menuName = "Config/ChessConfig")]
public class ChessConfig : ScriptableObject
{
    public List<Chess> ChessList;
}
