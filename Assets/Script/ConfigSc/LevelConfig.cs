using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class levelData
{
    public int level;
    public int CoinGet;
    public List<string> IdEnemys;
}


[CreateAssetMenu(fileName ="LeveConfig", menuName ="Config/LevelConfig")]
public class LevelConfig : ScriptableObject
{
    public List<levelData> levelDatas;
}
