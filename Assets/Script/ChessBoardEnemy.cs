using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessBoardEnemy : ChessBoard
{
    private float TotalhealthEnemy;
    [SerializeField] private List<GameObject> slots = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        chessBoardLogic.StartGameCallBack += startgame;
        chessBoardLogic.UpdateBoardEnemyCallBack += UpdateDataChessBoardEnemy;
        chessBoardLogic.UpdateHpBoardEnemyCallBack += updateTotalhealthEnemy;
        chessBoardLogic.DestroyAllPrefabCallBack += DesTroyAllPrefabs;
        var Layer = layer.Instance;
        Setup(slots);
        setDataChess(chessBoardLogic.getDataChessEnemy(), slots , Layer.numberLayerEnemy , Layer.League);
    }

    public void startgame(bool isStartGame)
    {
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        foreach (var item in slots)
        {
            ChessItem chessItem = item.GetComponent<ChessItem>();
            chessItem.isStartGame(isStartGame);
        }
        TotalhealthEnemy = setTotalHealth(chessBoardLogic.getDataChessEnemy());
    }


    public void UpdateDataChessBoardEnemy()
    {
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        var Layer = layer.Instance;
        setDataChess(chessBoardLogic.getDataChessEnemy(), slots, Layer.numberLayerEnemy, Layer.League);
    }
    public void updateTotalhealthEnemy(float hp)
    {
        TotalhealthEnemy -= hp;
        if (TotalhealthEnemy <= 0)
        {
            GameManager.Instance.EndFight(true);
        }
    }

    public void DesTroyAllPrefabs()
    {
        DesTroyAllGameobject(slots);
    }
}
