using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiController : SingletonMono<UiController>
{
    [SerializeField] private Button btnCharacterList,btnAddDinosaurs,btnStart,BtnAddWarriors;
    [SerializeField] private CharacterHandle PopCharacters;
    [SerializeField] private PopUpNewChessHandle PopUpNewChess;
    [SerializeField] private PopupEnd PopupEndFight;
    [SerializeField] private TMP_Text txtCoin, Txtlevel;
    [SerializeField] private GameObject mainUI;
    private GameManager gameManager;
    private void Start()
    {
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        var playerLogic = Logic.Instance.PlayerLogic;
        chessBoardLogic.PopupNewChessCallBack += showPopNewChess;
        btnCharacterList.onClick.AddListener(characterListClick);
        btnAddDinosaurs.onClick.AddListener(AddDinosaurs);
        BtnAddWarriors.onClick.AddListener (AddWarriors);
        btnStart.onClick.AddListener(startGame);
        playerLogic.UpdateDataPlayerCallBack += updateDataPlayer;

        updateDataPlayer(playerLogic.Coin, playerLogic.level);
    }
    private void OnDisable()
    {
        btnCharacterList.onClick.RemoveAllListeners();
        btnAddDinosaurs.onClick.RemoveAllListeners();
        BtnAddWarriors.onClick.RemoveAllListeners();
        btnStart.onClick.RemoveAllListeners();
    }


    private void characterListClick()
    {
        PopCharacters.gameObject.SetActive(true);
        Logic.Instance.ChessBoardLogic.hideChess();
    }

    public void ClosePopCharacters()
    {
        PopCharacters.gameObject.SetActive(false);
    }

    public void AddDinosaurs()
    {
        Logic.Instance.ChessBoardLogic.AddChess(0);
    }
    public void AddWarriors()
    {
        Logic.Instance.ChessBoardLogic.AddChess(1);
    }

    public void startGame()
    {
        GameManager.Instance.StartGame(true);
        MainUi(false);
    }
    public void showPopNewChess(Chess dataNewChess)
    {
       
        PopUpNewChess.gameObject.SetActive(true);
        PopUpNewChess.GetComponent<PopUpNewChessHandle>().setDataNewChess(dataNewChess);
        Logic.Instance.ChessBoardLogic.hideChess();
    }

    public void ShowPopupWin(bool isWin)
    {
        PopupEndFight.gameObject.SetActive(true);
        PopupEndFight.GetComponent<PopupEnd>().checkWinLose(isWin);
    }

    public void updateDataPlayer(int dataCoin, int dataLevel)
    {
        txtCoin.text = "Coin: " + dataCoin.ToString();
        Txtlevel.text = "Level: " +  dataLevel.ToString();
    }

    public void MainUi(bool isActieve)
    {
        mainUI.gameObject.SetActive(isActieve);
    }
}
