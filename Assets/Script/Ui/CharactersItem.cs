using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharactersItem : MonoBehaviour
{
    [SerializeField] private TMP_Text txtDamage, txtHealth,txtName;
    [SerializeField] private Image imgChess;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setData(Chess DataChess)
    {
        txtDamage.text  = DataChess.Damage.ToString();
        txtHealth.text = DataChess.Health.ToString();
        if(DataChess.Status == Status.Lock)
        {
            txtName.text = "Locked";
            imgChess.color = new Color32(0, 0, 0, 255);
        }
        else
        {
            txtName.text = DataChess.Id;
        }
        foreach(var item in ConfigManager.Instance.IconConfig.icons)
        {
            if(DataChess.Id == item.name)
            {
                imgChess.sprite = item.spriteIcon;                
            }
        }
    }
}
