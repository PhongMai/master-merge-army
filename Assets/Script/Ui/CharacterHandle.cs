using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


/*public enum characterListStatus
{
    Dinosaurs,
    Warriors
}*/

public class CharacterHandle : MonoBehaviour
{
    [SerializeField] private Button btnExit,btnDinosaurs,btnWarriors;
    [SerializeField] private Image imageHightLight;
    [SerializeField] private GameObject characterItemPrefab;
    [SerializeField] private GameObject parent;
    private List<CharactersItem> charactersItems = new List<CharactersItem>();
    private void OnEnable()
    {
        btnExit.onClick.AddListener(ClosePopCharacters);
        btnDinosaurs.onClick.AddListener(DinosaursOnClick);
        btnWarriors.onClick.AddListener(WarriorsOnClick);

        var dinosaursLogic = Logic.Instance.DinosaurLogic;

        updateCharacterList(dinosaursLogic.getData());
    }

    private void OnDisable()
    {
        btnExit.onClick.RemoveAllListeners();
        btnDinosaurs.onClick.RemoveAllListeners();
        btnWarriors.onClick.RemoveAllListeners();
    }

    public void updateCharacterList(List<Chess> chesses)
    {
        foreach(var item in charactersItems)
        {
            Destroy(item.gameObject);
        }
        charactersItems.Clear();


        for (int i = 0; i < chesses.Count; i++) 
        {
            GameObject itemPrefab = Instantiate(characterItemPrefab, parent.transform);
            CharactersItem item = itemPrefab.GetComponent<CharactersItem>();
            item.setData(chesses[i]);
            charactersItems.Add(item);
        }
    }

    public void DinosaursOnClick()
    {
        imageHightLight.gameObject.transform.DOMoveX(btnDinosaurs.transform.position.x, 0.5f);
        var dataDinosaurs = Logic.Instance.DinosaurLogic.getData();
        updateCharacterList(dataDinosaurs);
    }

    public void WarriorsOnClick()
    {
        imageHightLight.gameObject.transform.DOMoveX(btnWarriors.transform.position.x, 0.5f);
        var dataWarriors = Logic.Instance.WarriorLogic.getData();
        updateCharacterList(dataWarriors);
    }

    public void ClosePopCharacters()
    {
        UiController.Instance.ClosePopCharacters();
        Logic.Instance.ChessBoardLogic.resetDataChessBoard(false);
    }
}
