using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopUpNewChessHandle : MonoBehaviour
{
    [SerializeField] TMP_Text txtHp, txtDamage, txtName;
    [SerializeField] Image imgIcon;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setDataNewChess(Chess dataNewChess)
    {
        txtHp.text = dataNewChess.Health.ToString();
        txtDamage.text = dataNewChess.Damage.ToString();
        txtName.text = dataNewChess.Id.ToString();
        foreach(var itemm in ConfigManager.Instance.IconConfig.icons)
        {
            if(dataNewChess.Id == itemm.name)
            {
                imgIcon.sprite = itemm.spriteIcon;
            }
        }
    }

    public void ClosePopup()
    {        
        gameObject.SetActive(false);
        Logic.Instance.ChessBoardLogic.resetDataChessBoard(false);
    }
}
