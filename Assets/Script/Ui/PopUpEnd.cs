using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupEnd : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Image imageWin,imgLose;
    [SerializeField] private Button btnNewStart;
    private bool isWin;

    private void OnEnable()
    {
        btnNewStart.onClick.AddListener(newStart);
        btnNewStart.gameObject.SetActive(false);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void checkWinLose(bool data)
    {
        isWin = data;
        imageWin.gameObject.SetActive(isWin);
        imgLose.gameObject.SetActive(!isWin);
        StartCoroutine(Wainter());        
    }

    public void newStart()
    {
        gameObject.SetActive(false);
        Logic.Instance.ChessBoardLogic.resetDataChessBoard(isWin);
        UiController.Instance.MainUi(true);
    }

    IEnumerator Wainter()
    {
        yield return new WaitForSeconds(2);
        Logic.Instance.ChessBoardLogic.DestroyAllPrefab();
        btnNewStart.gameObject.SetActive(true);
    }
}
