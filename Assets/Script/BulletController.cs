using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class BulletController : MonoBehaviour
{
    private Vector3 PosTarget;
    private int LayerPlayer;
    private float damage;
    [SerializeField] private float speed;
    void Start()
    {
        
    }
    void Update()
    {
        transform.position = Vector2.Lerp(transform.position, PosTarget,speed *  Time.deltaTime);

        if(transform.position == PosTarget)
        {
            Destroy(gameObject);
        }
    }

    public void setDataTarget(Vector3 DataPosTarget ,int layerTarget , float dataDamage)
    {
        PosTarget = DataPosTarget;
        LayerPlayer = layerTarget;
        damage = dataDamage;

        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerPlayer && collision.gameObject.layer != gameObject.layer)
        {
                collision.gameObject.GetComponent<ChessController>().takeDamage(damage);
                Destroy(gameObject);                    
        }
    }
}
