using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class layer : Singleton<layer>
{
    public string Enemy => LayerMask.LayerToName(6);
    public int numberLayerEnemy => LayerMask.NameToLayer(Enemy);
    public string League => LayerMask.LayerToName(7);
    public int numberLayerLeague => LayerMask.NameToLayer(League);
}
