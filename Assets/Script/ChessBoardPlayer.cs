using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChessBoardPlayer : ChessBoard
{
    private float TotalhealthPlayer;
    public List<GameObject> slots = new List<GameObject>();
    private void Start()
    {
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        var Layer = layer.Instance;
        chessBoardLogic.UpdateDataChessBoardCallBack += UpdateDataChessBoard;
        chessBoardLogic.StartGameCallBack += startgame;
        chessBoardLogic.UpdateHpBoardPlayerCallBack += updateTotalhealthPlayer;
        chessBoardLogic.DestroyAllPrefabCallBack += DesTroyAllPrefabs;
        Setup(slots);
        setDataChess(chessBoardLogic.getData() , slots , Layer.numberLayerLeague , Layer.Enemy);
    }
    public void UpdateDataChessBoard(GameObject chessInMouse)
    {
        if(chessInMouse != null)
        {
            Destroy(chessInMouse);
        }      
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        var Layer = layer.Instance;
        setDataChess(chessBoardLogic.getData(), slots, Layer.numberLayerLeague, Layer.Enemy);
    }


    public void startgame(bool isStartGame)
    {
        foreach (var item in slots)
        {
            ChessItem chessItem = item.GetComponent<ChessItem>();
            chessItem.isStartGame(isStartGame);
        }
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        TotalhealthPlayer = setTotalHealth(chessBoardLogic.getData());
    }
    public void updateTotalhealthPlayer(float hp)
    {
        TotalhealthPlayer -= hp;
        if (TotalhealthPlayer <= 0)
        {
            GameManager.Instance.EndFight(false);
        }
    }

    public void DesTroyAllPrefabs()
    {
        DesTroyAllGameobject(slots);
    }
}
