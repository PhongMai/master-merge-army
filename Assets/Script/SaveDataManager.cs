using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SaveDataManager : MonoBehaviour
{

    public SaveChessBoard dataChessBoard = new SaveChessBoard();
    public SaveDataSoldiders dataSoldiers = new SaveDataSoldiders();
    public SaveDataPlayer dataPlayer = new SaveDataPlayer();
    private T LoadData<T> (T baseData) where T : BaseSaveData
    {
        string jsonData = PlayerPrefs.GetString(baseData.getKey());
        if (string.IsNullOrEmpty(jsonData))
        {
            baseData.LoadDefaunt();
        }
        else
        {
            baseData = JsonUtility.FromJson<T>(jsonData);
        }

        baseData.LoadAfter();
        return baseData;
    }

    public void Load()
    {
        dataChessBoard = LoadData(dataChessBoard);
        dataSoldiers = LoadData(dataSoldiers);    
        dataPlayer = LoadData(dataPlayer);
    }


    public void SaveDataSlot(List<Chess> chessOnBoard, List<Chess> chessBoardEnemy)
    {
        dataChessBoard.slots = chessOnBoard;
        dataChessBoard.EnemySlots = chessBoardEnemy;
        dataChessBoard.SaveData();
    }

    public void SaveDataSoldiers(List<Chess> soldiers)
    {
        dataSoldiers.ListSoldiers = soldiers;
        dataSoldiers.SaveData();
    }

    public void setDataPlayer(int datalevel, int dataCoin)
    {
        dataPlayer.level = datalevel;
        dataPlayer.Coin = dataCoin;
        dataPlayer.SaveData();
    }
}
