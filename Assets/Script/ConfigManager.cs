using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigManager : SingletonMono<ConfigManager>
{
    [SerializeField] private ChessConfig dinosaurConfig;
    [SerializeField] private ChessConfig warriorConfig;
    [SerializeField] private IconConfig iconConfig;
    [SerializeField] private LevelConfig levelConfig;

    public ChessConfig DinosaurConfig => dinosaurConfig;
    public ChessConfig WarriorConfig => warriorConfig;

    public IconConfig IconConfig => iconConfig;

    public LevelConfig LevelConfig => levelConfig;
}
