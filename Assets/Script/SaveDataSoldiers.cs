using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataSoldiders : BaseSaveData
{

    public List<Chess> ListSoldiers = new List<Chess>();
    public override string getKey()
    {
        return "W_Soldiers";
    }

    public override void LoadDefaunt()
    {
        ListSoldiers = new List<Chess>();
    }


}
