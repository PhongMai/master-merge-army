using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ChessButton : MonoBehaviour, IUpdateSelectedHandler, IPointerDownHandler,IPointerUpHandler, IPointerEnterHandler
{
    bool isHold;

    public void OnPointerDown(PointerEventData eventData)
    {
        isHold = true;
        gameObject.GetComponent<Image>().color = Color.red;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        
        var chessItem = gameObject.GetComponent<ChessItem>();

        RectTransform reactPoint = transform.GetComponent<RectTransform>();
        Vector2 pos = new Vector2(reactPoint.position.x, reactPoint.position.y);
        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        chessBoardLogic.setNumberAfter(chessItem.numberSlot);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isHold= false;
        gameObject.GetComponent<Image>().color = Color.white;
        if(gameObject.GetComponent<ChessItem>().numberSlot % 2 == 0 )
        {
            gameObject.GetComponent<Image>().color =new Color32(164 , 164,164,255);
        }

        var chessBoardLogic = Logic.Instance.ChessBoardLogic;
        chessBoardLogic.MoveChessWithSlot();
    }

    public void OnUpdateSelected(BaseEventData eventData)
    {
        if(isHold == true)
        {
            var chessBoardLogic = Logic.Instance.ChessBoardLogic;
            var chessItem = gameObject.GetComponent<ChessItem>();
            if(chessItem.chessCurrent != null)
            {
                chessBoardLogic.MoveChessWithMouse(chessItem.gameObjectCurrent);                
                chessBoardLogic.setNumberBefore(chessItem.numberSlot);
            }
        }
    }

}
