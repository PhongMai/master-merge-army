using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


public class ChessItem : MonoBehaviour
{

    public Chess chessCurrent { get; private set; }
    public GameObject gameObjectCurrent { get; private set ; }
    public int numberSlot { get; private set; }
    public void setid(int id)
    {
        numberSlot = id;
    }

    public void setData(Chess dataSlot, int layerchess, string layerTarget)
    {
        Destroy(gameObjectCurrent);
        if(dataSlot != null)
        {
            if (dataSlot.prefabs != null)
            {
                chessCurrent = dataSlot;
                gameObjectCurrent = Instantiate(dataSlot.prefabs.gameObject);
                ChessController chessController = gameObjectCurrent.GetComponent<ChessController>();
                chessController.setLayer(layerchess , layerTarget);
                chessController.setData(dataSlot.Health, dataSlot.Damage,dataSlot.DistanceAtt,dataSlot,dataSlot.Type);
                RectTransform objectA = transform.GetComponent<RectTransform>();
                Vector3 pos = new Vector3(objectA.position.x, objectA.position.y);
                gameObjectCurrent.transform.localPosition = pos;
            }
        }
    }

    public void isStartGame(bool startgame)
    {
        if(gameObjectCurrent != null)
        {
            ChessController chessController = gameObjectCurrent.GetComponent<ChessController>();
            chessController.setStatus(startgame);
        }        
    }

    public void DesTroyPrefab()
    {
        Destroy(gameObjectCurrent);
    }
}
