using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveChessBoard : BaseSaveData
{
    public List<Chess> slots = new List<Chess>();

    public List<Chess> EnemySlots = new List<Chess>();
    public override string getKey()
    {
        return "W_ChessBoard";
    }


    public override void LoadDefaunt()
    {
        slots = new List<Chess>();
        EnemySlots = new List<Chess>();
    }

}
